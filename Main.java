import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

class SatelliteData {
    private Date timestamp;
    private int satelliteId;
    private int redHighLimit;
    private int yellowHighLimit;
    private int yellowLowLimit;
    private int redLowLimit;
    private double rawValue;
    private String component;

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public int getSatelliteId() {
        return satelliteId;
    }

    public void setSatelliteId(int satelliteId) {
        this.satelliteId = satelliteId;
    }

    public int getRedHighLimit() {
        return redHighLimit;
    }

    public void setRedHighLimit(int redHighLimit) {
        this.redHighLimit = redHighLimit;
    }

    public int getYellowHighLimit() {
        return yellowHighLimit;
    }

    public void setYellowHighLimit(int yellowHighLimit) {
        this.yellowHighLimit = yellowHighLimit;
    }

    public int getYellowLowLimit() {
        return yellowLowLimit;
    }

    public void setYellowLowLimit(int yellowLowLimit) {
        this.yellowLowLimit = yellowLowLimit;
    }

    public int getRedLowLimit() {
        return redLowLimit;
    }

    public void setRedLowLimit(int redLowLimit) {
        this.redLowLimit = redLowLimit;
    }

    public double getRawValue() {
        return rawValue;
    }

    public void setRawValue(double rawValue) {
        this.rawValue = rawValue;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }
}

class AlertData {
    private int satelliteId;
    private String severity;
    private String component;
    private Date timestamp;

    public AlertData(int satelliteId, String severity, String component, Date timestamp) {
        this.satelliteId = satelliteId;
        this.severity = severity;
        this.component = component;
        this.timestamp = timestamp;
    }

    public int getSatelliteId() {
        return satelliteId;
    }

    public void setSatelliteId(int satelliteId) {
        this.satelliteId = satelliteId;
    }

    public String getSeverity() {
        return severity;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }
}

public class Main {
    public static void main(String[] args) {
        if (args.length < 1) {
            System.out.println("Please provide the input file path.");
            return;
        }

        String filename = args[0];

        try {
            List<SatelliteData> data = readDataFromFile(filename);
            List<AlertData> alerts = processSatelliteData(data);
            printAlerts(alerts);
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }

    private static List<SatelliteData> readDataFromFile(String filename) throws IOException, ParseException {
        List<SatelliteData> data = new ArrayList<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
            String line;
            while ((line = reader.readLine()) != null) {
                SatelliteData satelliteData = parseLine(line);
                data.add(satelliteData);
            }
        }

        return data;
    }

    private static SatelliteData parseLine(String line) throws ParseException {
        String[] parts = line.split("\\|");
        if (parts.length != 8) {
            throw new IllegalArgumentException("Invalid line format: " + line);
        }

        SatelliteData satelliteData = new SatelliteData();
        satelliteData.setTimestamp(parseDate(parts[0]));
        satelliteData.setSatelliteId(Integer.parseInt(parts[1]));
        satelliteData.setRedHighLimit(Integer.parseInt(parts[2]));
        satelliteData.setYellowHighLimit(Integer.parseInt(parts[3]));
        satelliteData.setYellowLowLimit(Integer.parseInt(parts[4]));
        satelliteData.setRedLowLimit(Integer.parseInt(parts[5]));
        satelliteData.setRawValue(Double.parseDouble(parts[6]));
        satelliteData.setComponent(parts[7]);

        return satelliteData;
    }

    private static Date parseDate(String dateString) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
        return dateFormat.parse(dateString);
    }

    private static List<AlertData> processSatelliteData(List<SatelliteData> data) {
        Map<Integer, LinkedList<SatelliteData>> batteryReadings = new HashMap<>();
        Map<Integer, LinkedList<SatelliteData>> thermostatReadings = new HashMap<>();
        List<AlertData> alerts = new ArrayList<>();

        for (SatelliteData satelliteData : data) {
            if ("BATT".equals(satelliteData.getComponent())) {
                LinkedList<SatelliteData> readings = batteryReadings.computeIfAbsent(satelliteData.getSatelliteId(), k -> new LinkedList<>());
                readings.add(satelliteData);

                if (readings.size() >= 3 && isWithinFiveMinuteInterval(readings)) {
                    boolean hasRedLowReadings = readings.stream()
                            .anyMatch(dataPoint -> dataPoint.getRawValue() < dataPoint.getRedLowLimit());

                    if (hasRedLowReadings) {
                        alerts.add(new AlertData(satelliteData.getSatelliteId(), "RED LOW", "BATT", satelliteData.getTimestamp()));
                    }
                }

                if (readings.size() > 3) {
                    readings.removeFirst();
                }
            } else if ("TSTAT".equals(satelliteData.getComponent())) {
                LinkedList<SatelliteData> readings = thermostatReadings.computeIfAbsent(satelliteData.getSatelliteId(), k -> new LinkedList<>());
                readings.add(satelliteData);

                if (readings.size() >= 3 && isWithinFiveMinuteInterval(readings)) {
                    boolean hasRedHighReadings = readings.stream()
                            .anyMatch(dataPoint -> dataPoint.getRawValue() > dataPoint.getRedHighLimit());

                    if (hasRedHighReadings) {
                        alerts.add(new AlertData(satelliteData.getSatelliteId(), "RED HIGH", "TSTAT", satelliteData.getTimestamp()));
                    }
                }

                if (readings.size() > 3) {
                    readings.removeFirst();
                }
            }
        }

        return alerts;
    }

    private static boolean isWithinFiveMinuteInterval(LinkedList<SatelliteData> readings) {
        SatelliteData firstDataPoint = readings.getFirst();
        SatelliteData lastDataPoint = readings.getLast();

        long intervalInMillis = lastDataPoint.getTimestamp().getTime() - firstDataPoint.getTimestamp().getTime();
        long fiveMinutesInMillis = 5 * 60 * 1000;

        return intervalInMillis <= fiveMinutesInMillis;
    }

    private static void printAlerts(List<AlertData> alerts) {
        for (AlertData alert : alerts) {
            System.out.println(formatAlert(alert));
        }
    }

    private static String formatAlert(AlertData alert) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        return "{\n" +
                "  \"satelliteId\": " + alert.getSatelliteId() + ",\n" +
                "  \"severity\": \"" + alert.getSeverity() + "\",\n" +
                "  \"component\": \"" + alert.getComponent() + "\",\n" +
                "  \"timestamp\": \"" + dateFormat.format(alert.getTimestamp()) + "\"\n" +
                "}";
    }
}
